package gs

import "strings"

func (str Str) ParseUri() (kargs Dict[Str]) {
	kargs = make(Dict[Str])
	str.Split("&").Every(func(no int, i Str) {
		if i.In("=") {
			fs := i.Split("=", 2)
			kargs[fs.Nth(0).Trim().Str()] = fs.Nth(1).Trim()
		}
	})
	return
}

func (str Str) ParseKV(usecolon ...bool) (kargs Dict[Str]) {
	kargs = make(Dict[Str])
	sep := "="
	if usecolon != nil && usecolon[0] {
		sep = ":"
	}
	if sep == "=" {
		str.Split(";").Every(func(no int, i Str) {

			if sep != "" {
				var k, val Str
				i.Split(sep, 2).Every(func(nx int, v Str) {
					// v.Println(nx)
					if nx == 0 {
						k = v.Trim()
					} else {
						val = v.Trim()
					}
				})
				if k != "" {
					kargs[k.String()] = val
				}
			}
		})
	} else {
		str.EveryLine(func(no int, i Str) {

			if sep != "" {
				var k, val Str
				i.Split(sep, 2).Every(func(nx int, v Str) {
					// v.Println(nx)
					if nx == 0 {
						k = v.Trim()
					} else {
						val = v.Trim()
					}
				})
				if k != "" {
					kargs[k.String()] = val
				}
			}
		})

	}
	return
}

func (str Str) Split(sep string, n ...int) (strs Strs) {
	ni := -1
	if n != nil {
		ni = n[0]
	}
	return strs.Adds(strings.SplitN(str.String(), sep, ni)...)
}

func (str Str) SplitSpace() (strs Strs) {
	return strs.Adds(strings.Fields(str.String())...)
}

func (str Str) Word() Str {
	e := str.Trim()
	if e.StartsWith("\"") && e.EndsWith("\"") || e.StartsWith("'") && e.EndsWith("'") || e.StartsWith("`") && e.EndsWith("`") {
		return e.Slice(1, -1)
	}
	return e
}
