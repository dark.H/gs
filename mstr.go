package gs

import "strings"

type MStr struct {
	Str
	Mark bool
}

type MStrs []MStr

func (Mstrs MStrs) Add(es ...MStr) MStrs {
	Mstrs = append(Mstrs, es...)
	return Mstrs
}

func (mstrs MStrs) Last() MStr {
	return mstrs[mstrs.Len()-1]
}

func (mstr MStr) MStrs() MStrs {
	return MStrs{mstr}
}

func (Mstrs MStrs) Len() int {
	return len(Mstrs)
}

func (mstrs MStrs) Strs() Strs {
	var es Strs
	mstrs.With(func(i MStr) MStr {
		es = es.Add(i.Str)
		return i
	})
	return es
}

func (Mstrs MStrs) Every(icall func(i MStr)) MStrs {
	for _, s := range Mstrs {
		icall(s)
	}
	return Mstrs
}

func (Mstrs MStrs) Filter(icall func(i MStr) bool) MStrs {
	es := MStrs{}
	for _, s := range Mstrs {
		if icall(s) {
			es = es.Add(s)
		}
	}
	return es
}

func (Mstrs MStrs) WithExpand(icall func(i MStr) MStrs) MStrs {
	es := MStrs{}
	for _, s := range Mstrs {
		ess := icall(s)
		es = es.Add(ess...)
	}
	return es
}

func (Mstrs MStrs) With(icall func(i MStr) MStr) MStrs {
	es := MStrs{}
	for _, s := range Mstrs {
		news := icall(s)
		es = es.Add(news)
	}
	return es
}

func (Mstrs MStrs) List() (e []string) {
	Mstrs.Every(func(i MStr) {
		e = append(e, i.String())
	})
	return
}

func (Mstrs MStrs) Join(sep Str) Str {
	return Str(strings.Join(Mstrs.List(), sep.String()))
}
