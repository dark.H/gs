package gs

import (
	"regexp"
)

func (str Str) Find(restr string, n ...int) (strs Strs) {
	r := regexp.MustCompile(restr)
	ni := -1
	if n != nil {
		ni = n[0]
	}

	if S(restr).In("(") && S(restr).In(")") {
		for _, vv := range r.FindAllStringSubmatch(str.String(), ni) {
			// fmt.Println(strings.Join(vv[1:], "||"))
			for _, x := range vv[1:] {
				strs = strs.Add(Str(x))
			}
		}

	} else {
		for _, i := range r.FindAllString(str.String(), ni) {
			strs = strs.Add(Str(i))
		}

	}
	return
}
