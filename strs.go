package gs

import (
	"fmt"
	"sort"
	"strings"
)

type Strs []Str

type CanString interface {
	String() string
}

// func (strs Strs)

func (strs Strs) Adds(es ...string) Strs {
	for _, i := range es {
		strs = append(strs, Str(i))
	}
	return strs
}

func (strs Strs) Add(es ...Str) Strs {
	for _, i := range es {
		strs = append(strs, i)
	}
	return strs
}

func (strs Strs) Copy() (nstrs Strs) {
	nstrs = append(nstrs, strs...)
	return
}

func (strs Strs) Last() Str {
	return strs[strs.Len()-1]
}

func (strs Strs) Len() int {
	return len(strs)
}
func (strs Strs) Every(icall func(no int, i Str)) Strs {
	for ix, s := range strs {
		icall(ix, s)
	}
	return strs
}

func (strs Strs) Filter(icall func(i Str) bool) Strs {
	es := Strs{}
	for _, s := range strs {
		if icall(s) {
			es = es.Add(s)
		}
	}
	return es
}

func (strs Strs) WithExpand(icall func(i Str) Strs) Strs {
	es := Strs{}
	for _, s := range strs {
		ess := icall(s)
		es = es.Add(ess...)
	}
	return es
}

func (strs Strs) With(icall func(ix int, i Str) Str) Strs {
	es := Strs{}
	for i, s := range strs {
		news := icall(i, s)
		es = es.Add(news)
	}
	return es
}

func (strs Strs) In(i Str) bool {
	for _, i2 := range strs {
		if i == i2 {
			return true
		}
	}
	return false
}

func (strs Strs) List() (e []string) {
	strs.Every(func(ix int, i Str) {
		e = append(e, i.String())
	})
	return
}

func (strs Strs) Join(sep Str) Str {
	return Str(strings.Join(strs.List(), sep.String()))
}

func (strs Strs) FromMapKeys(a map[string]string) Strs {

	for k := range a {
		strs = strs.Add(Str(k))
	}
	// fmt.Println(strs)
	return strs
}

func (strs Strs) Sort(s func(k1, k2 Str) bool) Strs {
	sort.Slice(strs, func(i, j int) bool {
		return s(strs[i], strs[j])
	})
	return strs
}

func (strs Strs) Nth(i int) Str {
	if i < len(strs) {
		return strs[i]
	} else {
		return Str("")
	}
}

func (strs Strs) InnertArrayByLen(l int) (ass List[Strs]) {
	e := Strs{}
	fmt.Println(0, e)

	strs.Every(func(no int, i Str) {
		fmt.Println(1, e)
		e = e.Add(i)
		fmt.Println(2, e, len(e))
		// if no%l != 0 {

		// 	e = e.Add(i)
		// 	fmt.Println(1, e)
		// } else {
		// 	fmt.Println(0, e)

		// 	if len(e) > 0 {

		// 		// ass = ass.Add(e.Copy())
		// 		// e = Strs{}
		// 	}
		// 	e = e.Add(i)
		// }
	})
	return
}

func (strs Strs) Any(fi func(item Str) bool) Str {
	// hit := Str("")
	for _, item := range strs {
		if fi(item) {
			return item
		}
	}
	return Str("")
}
