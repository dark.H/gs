package main

import (
	"fmt"

	"gitee.com/dark.H/gs"
)

func main() {

	fmt.Println(gs.GetWindowsSize())
	gs.Str("Hello world!").Repeat(2).Reverse().Println()
	gs.DEFAULT_PROGRESS_OPT.NormalStart(100, 1, func(now int64) (updatedNow int64) {
		return now + 3
	}).Wait()

}
