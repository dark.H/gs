package gs

import (
	"bytes"
	"os"
	"os/exec"
	"runtime"
)

var (
	Platform = runtime.GOOS
	PathSep  = _getPathSep()
)

func _getPathSep() string {
	if Platform == "windows" {
		return "\\"
	}
	return "/"
}

func (str Str) Platform() Str {
	return Str(runtime.GOOS)
}

func (str Str) Exec(typed ...string) Str {
	var args []string
	sep := "\n"
	if str.Platform() == "windows" {
		sep = "\r\n"
		args = []string{"C:\\Windows\\System32\\Cmd.exe", "/C"}
	} else {
		args = []string{"bash", "-c"}
	}
	PATH := os.Getenv("PATH")
	if PATH == "" {
		PATH = "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin"
	}
	args = append(args, str.String())
	cmd := exec.Command(args[0], args[1:]...)
	outbuffer := bytes.NewBuffer([]byte{})
	cmd.Stdout = outbuffer
	cmd.Stderr = outbuffer
	if typed != nil {
		ww := List[string](typed).Join(sep) + Str(sep)
		// ww.Println("s")
		inbuffer := bytes.NewBuffer(ww.Bytes())
		cmd.Stdin = inbuffer
	}
	cmd.Run()
	return Str(outbuffer.String())
}
